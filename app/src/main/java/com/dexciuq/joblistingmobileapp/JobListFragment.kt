package com.dexciuq.joblistingmobileapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearSnapHelper
import com.dexciuq.joblistingmobileapp.data.DataSource.jobListItems
import com.dexciuq.joblistingmobileapp.adapter.JobListAdapter
import com.dexciuq.joblistingmobileapp.databinding.FragmentJobListBinding
import com.dexciuq.joblistingmobileapp.decoration.OffsetDecoration
import com.dexciuq.joblistingmobileapp.decoration.StickyHeaderDecoration
import com.dexciuq.joblistingmobileapp.decoration.swipeToDismiss

class JobListFragment : Fragment() {

    private val binding by lazy { FragmentJobListBinding.inflate(layoutInflater) }
    private val offsetDecoration = OffsetDecoration(vertical = 6)
    private val stickyHeaderDecoration = StickyHeaderDecoration()
    private val snapHelper = LinearSnapHelper()
    private lateinit var adapter: JobListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        adapter = JobListAdapter {
            findNavController().navigate(JobListFragmentDirections.actionJobListFragmentToJobDetailsFragment(it))
        }
        return with(binding) {
            jobList.adapter = adapter
            binding.jobList.addItemDecoration(offsetDecoration)
            binding.jobList.addItemDecoration(stickyHeaderDecoration)
            snapHelper.attachToRecyclerView(binding.jobList)
            jobList.swipeToDismiss {
                Toast.makeText(requireContext(), jobListItems[it].toString(), Toast.LENGTH_SHORT).show()
            }
            adapter.submitList(jobListItems)
            root
        }
    }

    override fun onDestroyView() {
        binding.jobList.removeItemDecoration(offsetDecoration)
        binding.jobList.removeItemDecoration(stickyHeaderDecoration)
        super.onDestroyView()
    }
}