package com.dexciuq.joblistingmobileapp

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.setupWithNavController
import com.dexciuq.joblistingmobileapp.data.DataSource.jobListItems
import com.dexciuq.joblistingmobileapp.databinding.FragmentJobDetailsBinding
import com.dexciuq.joblistingmobileapp.databinding.FragmentWelcomeBinding
import com.dexciuq.joblistingmobileapp.model.JobListItem

class JobDetailsFragment : Fragment() {

    private val binding by lazy { FragmentJobDetailsBinding.inflate(layoutInflater) }
    private val args: JobDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return with(binding) {
            toolbar.setNavigationOnClickListener {
                findNavController().navigateUp()
            }
            val job = jobListItems[args.id] as JobListItem.Job

            name.text = job.name
            company.text = job.company
            salary.text = requireContext().getString(R.string.salary, job.salaryPerYearRangeInK.first, job.salaryPerYearRangeInK.second)

            root
        }
    }
}