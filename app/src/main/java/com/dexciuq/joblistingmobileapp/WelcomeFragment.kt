package com.dexciuq.joblistingmobileapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.dexciuq.joblistingmobileapp.data.DataSource.welcomeInfoItems
import com.dexciuq.joblistingmobileapp.adapter.WelcomeInfoAdapter
import com.dexciuq.joblistingmobileapp.databinding.FragmentWelcomeBinding
import com.dexciuq.joblistingmobileapp.decoration.OffsetDecoration

class WelcomeFragment : Fragment() {

    private val binding by lazy { FragmentWelcomeBinding.inflate(layoutInflater) }
    private val adapter = WelcomeInfoAdapter(welcomeInfoItems)
    private val offsetDecoration = OffsetDecoration(vertical = 8)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setupWelcomeInfoRecyclerView()
        setupGetStartedButton()
        return binding.root
    }

    private fun setupGetStartedButton() {
        binding.getStartedButton.setOnClickListener {
            findNavController().navigate(R.id.action_welcomeFragment_to_jobListFragment)
        }
    }

    private fun setupWelcomeInfoRecyclerView() {
        binding.welcomeInfoRv.adapter = adapter
        binding.welcomeInfoRv.addItemDecoration(offsetDecoration)
        binding.welcomeInfoRv.setHasFixedSize(true)
    }

    override fun onDestroyView() {
        binding.welcomeInfoRv.removeItemDecoration(offsetDecoration)
        super.onDestroyView()
    }
}