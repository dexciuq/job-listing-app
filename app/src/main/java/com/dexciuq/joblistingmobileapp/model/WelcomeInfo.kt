package com.dexciuq.joblistingmobileapp.model

import androidx.annotation.DrawableRes

data class WelcomeInfo(
    @DrawableRes val icon: Int,
    val description: String,
)