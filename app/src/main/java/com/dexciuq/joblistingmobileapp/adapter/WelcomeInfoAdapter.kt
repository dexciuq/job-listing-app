package com.dexciuq.joblistingmobileapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dexciuq.joblistingmobileapp.databinding.ItemWelcomeInfoBinding
import com.dexciuq.joblistingmobileapp.model.WelcomeInfo

class WelcomeInfoAdapter(
    private val items: List<WelcomeInfo>
) : RecyclerView.Adapter<WelcomeInfoAdapter.WelcomeInfoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = WelcomeInfoViewHolder(
        ItemWelcomeInfoBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    )

    override fun onBindViewHolder(holder: WelcomeInfoViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    inner class WelcomeInfoViewHolder(
        private val binding: ItemWelcomeInfoBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(welcomeInfo: WelcomeInfo) {
            binding.infoIcon.setImageResource(welcomeInfo.icon)
            binding.infoText.text = welcomeInfo.description
        }
    }
}